<?php

namespace App\Http\Controllers\Article;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use App\Models\Tag;
use Laracasts\Flash\Flash;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $article = Article::all();
        $tag_article = Tag::orderBy('id','ASC')->pluck('name', 'id')->toArray();
        $category = Category::orderBy('id','ASC')->pluck('name', 'id')->toArray();
        $categoria = Category::orderBy('name','ASC')->get();
        $tags = Tag::orderBy('name','ASC')->get();

        

        return view('articles')
        ->with('article', $article)
        ->with('tag_article', $tag_article)
        ->with('category', $category)
        ->with('categoria', $categoria)
        ->with('tags', $tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = new Article($request->all());
        //dd($request->tag);
        $article->save();

        $article->tags()->sync($request->tag);

        return redirect()->route('Article.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $tag = Tag::orderBy('id','ASC')->pluck('name', 'id')->toArray();
        $category = Category::orderBy('id','ASC')->pluck('name', 'id')->toArray();

        return view('edit.article')
        ->with('article', $article)
        ->with('tag', $tag)
        ->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::find($id);
        $article->fill($request->all());
        //dd($portafolio);
        $article->save();

        $article->tags()->sync($request->tag);

        return redirect()->route('Article.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        $article->delete();


        return redirect()->route('Article.index');
    }
    public function searchCategory($name){
        $category = Category::SearchCategory($name)->first();
        $article = $category->articles;
        dd($article);
    }
}
