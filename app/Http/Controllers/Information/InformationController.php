<?php

namespace App\Http\Controllers\Information;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Information;
use Carbon\Carbon;

class InformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct(){
        Carbon::setLocale('es');
    }

    public function index()
    {
        $information = Information::all()->first();

        //dd($information);

        return view('information')
        ->with('information', $information);
    }


    public function update(Request $request, $id)
    {
        $information = Information::find($id);
        $information->fill($request->all());
        //dd($request->all());
        $information->save();
        

        
        return redirect()->route('Information.index');
    }
}
