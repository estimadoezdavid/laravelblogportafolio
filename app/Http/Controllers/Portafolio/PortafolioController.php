<?php

namespace App\Http\Controllers\Portafolio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Portafolio;
use App\Models\Image;
use Laracasts\Flash\Flash;
use Carbon\Carbon;
use App\Http\Requests\PortafolioRequest;

class PortafolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct(){
        Carbon::setLocale('es');
    }

    
    public function index()
    {
        $portafolio = Portafolio::all();
        $image = Image::all();
        
        return view('portafolio')
        ->with('portafolio', $portafolio)
        ->with('image', $image);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PortafolioRequest $request)
    {
        $portafolio = new Portafolio($request->all());
        //dd($request->all());
        $portafolio->save();

        //Imagenes
        $file = $request->file('image');
        $name = 'blogjeisson_' . time() . '.' .$file->getClientOriginalExtension();
        $path = public_path() . '/images/portafolio/';
        $file->move($path, $name);

        $image = new Image();
        $image->name = $name;
        $image->portafolio()->associate($portafolio);
        $image->save();

        
        return redirect()->route('Portafolio.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = Image::find($id);

        return view('edit.portafolio')
        ->with('image', $image);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = Image::find($id);
        $portafolio = Portafolio::find($image->portafolio->id);
        $portafolio->fill($request->all());
        //dd($portafolio);
        $portafolio->save();

    
        return redirect()->route('Portafolio.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = Image::find($id);
        $portafolio = Portafolio::find($image->portafolio->id);
        $image->delete();
        $portafolio->delete();

        
        return redirect()->route('Portafolio.index');
    }
}
