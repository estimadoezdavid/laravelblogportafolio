<?php

namespace App\Http\Controllers\Tag;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tag;
use Laracasts\Flash\Flash;
use Carbon\Carbon;

class TagController extends Controller
{
    public function __construct(){
        Carbon::setLocale('es');
    }
    
    public function index()
    {
        $tag = Tag::all();

        return view('tags')
        ->with('tag', $tag);
    }

    public function store(Request $request)
    {
        $tag = new Tag($request->all());
        //dd($request->all());
        $tag->save();


        return redirect()->route('Tag.index');
    }

    public function edit($id)
    {
        $tag = Tag::find($id);

        return view('edit.tag')
        ->with('tag', $tag);
    }

    public function update(Request $request, $id)
    {
        $tag = Tag::find($id);
        $tag -> fill($request->all());
        $tag->save();

        return redirect()->route('Tag.index');
    }

    public function destroy($id)
    {
        $tag = Tag::find($id);
        $tag->delete();

        return redirect()->route('Tag.index');
    }
}
