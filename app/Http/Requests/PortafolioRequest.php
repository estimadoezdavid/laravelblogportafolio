<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PortafolioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_project' => 'min:4|max:1000|required|',
            'description_project' => 'min:4|max:1000|required|',
            'image' => 'mimes:jpeg,jpg,png|image|min:10|max:2000'


        ];
    }
}
