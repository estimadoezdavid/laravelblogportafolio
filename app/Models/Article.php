<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table ="articles";
    protected $fillable = ['name','description','category_id','blog_id'];
    protected $guarded = ["id"];


    public function blog()
    {
        return $this->belongsTo('App\Models\Blog','blog_id','id');
    }

    public function category(){
        return $this->belongsTo('App\Models\Category','category_id','id');
    }


    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag')->withTimestamps();
    }

    
}
