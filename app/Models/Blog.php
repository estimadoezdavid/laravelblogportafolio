<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table ="blogs";
    protected $fillable = ['user_id'];
    protected $guarded = ["id"];


    public function user(){
        return $this->belongsTo('App\User');
    }
    public function articles(){
         return $this->hasMany('App\Models\Article','blog_id','id');
    }
    public function portafolios(){
         return $this->hasMany('App\Models\Portafolio','blog_id','id');
    }
    public function informations(){
         return $this->hasMany('App\Models\Information','blog_id','id');
    }
}
