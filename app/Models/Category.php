<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table ="categories";
    protected $fillable = ['name'];
    protected $guarded = ["id"];

    public function articles(){
         return $this->hasMany('App\Models\Article','category_id','id');
    }

    public function scopeSearchCategory($query, $name){
            return $query->where('name', '=', $name);
    }
}
