<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table ="images";
    protected $fillable = ['name','portafolio_id'];
    protected $guarded = ["id"];


    public function portafolio()
    {
        return $this->belongsTo('App\Models\Portafolio','portafolio_id','id');
    }
}
