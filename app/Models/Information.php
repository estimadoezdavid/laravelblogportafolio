<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    protected $table ="information";
    protected $fillable = ['personal_information','acedemic_information','more_informacion','blog_id'];
    protected $guarded = ["id"];


    public function blog()
    {
        return $this->belongsTo('App\Models\Blog','blog_id','id');
    }


}
