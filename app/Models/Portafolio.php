<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portafolio extends Model
{
    protected $table ="portafolios";
    protected $fillable = ['name_project','description_project', 'url','blog_id'];
    protected $guarded = ["id"];


    public function blog()
    {
        return $this->belongsTo('App\Models\Blog','blog_id','id');
    }
    public function images(){
         return $this->hasMany('App\Models\Image','portafolio_id','id');
    }
}
