@extends('layouts.app')

@section('header')

@endsection

@section('content')
@if (Auth::guest())
    @else
        <div class="pull-right">
             <a data-toggle="modal" data-target="#New" data-placement="bottom" title="Nuevo"><i class="btn fa fa-plus fa-4x" aria-hidden="true"></i></a>
        </div>
@endif
    <section class="main-section" id="service">
            <center>
                <h1>Articulos</h1>    
            </center>        
            

        <div class="container">
            <div class="row">
                <div class="col-md-8">
        
                    
                @foreach ($article as $article)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{$article->name}}
                            <div class="pull-right"> Tags:
                                @foreach ($article->tags as $tag)
                                    {{$tag->name}}.
                                @endforeach
                            </div>
                        </div>
                            <div class="panel-body">
                                <h3>{!! $article->description!!}</h3>
                            </div>
                            <div class="panel-footer">
                                <div class="pull-right">
                                    {!! $article->updated_at->diffForHumans() !!}
                                </div>
                                @if (Auth::guest())
                                <p>Categoria:  {{$article->category->name}}</p>
                                @else
                                <p>Categoria:  {{$article->category->name}}</p>
                                    <center>

                                        <a href="{{route('Article.edit', $article->id)}}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="btn fa fa-pencil fa-2x" aria-hidden="true"></i></a>
                                        <a href="{{route('Article.destroy', $article->id)}}" data-toggle="tooltip" data-placement="bottom" title="Eliminar"><i class="btn fa fa-trash fa-2x" aria-hidden="true"></i></a>
                                    </center>
                                @endif
                                
                            </div>
                        </div>

                @endforeach
                </div>
                
                <div class="col-md-4">
                    <div class="panel panel-primary">
                      <div class="panel-heading">Categorias</div>
                      <div class="panel-body">
                        @foreach ($categoria as $categoria)
                        <li class="list-group-item">
                            <a href="{{ route('Category.search', $categoria->name) }}">
                                {!! $categoria->name !!}
                            </a>
                            
                            <span class="badge">{!! $categoria -> articles -> count() !!}</span>
                        </li>
                        
                        @endforeach
                      </div>
                    </div>

                    <div class="panel panel-info">
                      <div class="panel-heading">Tags</div>
                      <div class="panel-body">
                        @foreach ($tags as $tags)
                        <a href="{{ route('Tag.search', $tags->name) }}">
                            <span class="badge">
                                {!! $tags -> name !!}
                            </span>
                        </a>
                        @endforeach
                      </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

<!--
    Mis Modelas
-->
<!-- Agregar al portafolio -->
  <div class="modal fade" id="New" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Nuevo articulo</h4>
        </div>
        <div class="modal-body">
            {!! Form::open(['route' => 'Article.store', 'method' => 'POST', 'files' => true]) !!}
                

                <div class="form-group">
                    {!! Form::label('name_project', 'Nombre') !!}
                    {!! Form::text('name', null, ['class' =>'form-control textarea-personal', 'requerid', 'placeholder' => 'Nombre del articulo']) !!}
                </div>
                                        
                <div class="form-group">
                    {!! Form::label('description_project', 'Descripcion') !!}
                    {!! Form::textarea('description', null, ['class' =>'form-control textarea-academic', 'requerid', 'placeholder' => 'Descripcion del articulo']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('category_id', 'Prioridad',[ 'class' => 'col-md-4 control-label']) !!}
                    {!! Form::select('category_id', $category, null, ['class' => 'form-control category-select ','placeholder' => 'Seleccione una categoria', 'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('fk_priority_id', 'Requisitos de la historia',[ 'class' => ' control-label']) !!}
                    {!! Form::select('tag[]', $tag_article, null, ['class' => 'form-control tag-select ','placeholder' => 'Seleccione algunos tags', 'required','multiple']) !!}
                </div>
                <div class="form-group">
                    {!! Form::hidden('blog_id', 1) !!}
                </div>
        </div>
        <div class="modal-footer">
            <center>
                {!! Form::submit('Guardar', ['class' =>'btn btn-success']) !!}
            </center>
          {!! Form::close() !!}
        </div>
      </div>
      
    </div>
  </div>

@endsection

@section('js')
<script>
    $('#New').on('shown.bs.modal', function () {
        $('.tag-select', this).chosen({
        });

        $('.category-select', this).chosen({
        }); 
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $('.textarea-academic').trumbowyg();
</script>
@endsection