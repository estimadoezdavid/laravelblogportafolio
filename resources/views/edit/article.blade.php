@extends('layouts.app')

@section('header')

@endsection

@section('content')
<section class="main-section" id="service">

                    <div class="container">
                        
                            {!! Form::open(['route' => ['Article.update', $article->id], 'method' => 'PUT', 'files' => true]) !!}

                                <div class="form-group">
                                    {!! Form::label('name_project', 'Nombre') !!}
                                    {!! Form::text('name', $article->name, ['class' =>'form-control textarea-personal', 'requerid', 'placeholder' => 'Nombre del articulo']) !!}
                                </div>
                                                        
                                <div class="form-group">
                                    {!! Form::label('description_project', 'Descripcion') !!}
                                    {!! Form::textarea('description', $article->description, ['class' =>'form-control textarea-academic', 'requerid', 'placeholder' => 'Descripcion del articulo']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('category_id', 'Prioridad',[ 'class' => 'col-md-4 control-label']) !!}
                                    {!! Form::select('category_id', $category, $article->category_id, ['class' => 'form-control category-select ','placeholder' => 'Seleccione una categoria', 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('fk_priority_id', 'Requisitos de la historia',[ 'class' => ' control-label']) !!}
                                    {!! Form::select('tag[]', $tag, $article->tags, ['class' => 'form-control tag-select ','placeholder' => 'Seleccione algunos tags', 'required','multiple']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::hidden('blog_id', 1) !!}
                                </div>

                                <center>
                                    {!! Form::submit('Guardar', ['class' =>'btn btn-success']) !!}
                                </center>      
                            {!! Form::close() !!}
                    </div>   
    </section>
@endsection

@section('js')
<script>
        $('.tag-select', this).chosen({
        });

        $('.category-select', this).chosen({
        }); 
        
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $('.textarea-academic').trumbowyg();
</script>
@endsection