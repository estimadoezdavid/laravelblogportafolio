@extends('layouts.app')

@section('header')

@endsection

@section('content')
<section class="main-section" id="service">
         @if (Auth::guest())
                    <div class="container">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3><a href="#">{{$image->portafolio->name_project}}</a></h3>
                            </div>
                            <div class="panel-body">
                                <center><img src="/images/portafolio/{{$image->name}}" ></center>
                                <div>
                                    <h4>{{$image->portafolio->description_project}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="container">
                        @include('flash::message')
                            {!! Form::open(['route' => ['Portafolio.update', $image->id], 'method' => 'PUT', 'files' => true]) !!}
                                <div class="form-group">
                                    {!! Form::label('name_project', 'Nombre') !!}
                                    {!! Form::text('name_project', $image->portafolio->name_project, ['class' =>'form-control textarea-personal', 'requerid', 'placeholder' => 'Nombre del portafolio']) !!}
                                </div>                  
                                <div class="form-group">
                                    {!! Form::label('description_project', 'Descripcion del portafolio') !!}
                                    {!! Form::textarea('description_project', $image->portafolio->description_project, ['class' =>'form-control textarea-academic', 'requerid', 'placeholder' => 'Descripcion del portafolio']) !!}
                                </div>
                                <div>
                                    <center><img src="/images/portafolio/{{$image->name}}" width="1000" ></center>
                                </div>
                                <div class="form-group">
                                    {!! Form::hidden('blog_id', 1) !!}
                                </div>

                                <center>
                                    {!! Form::submit('Guardar', ['class' =>'btn btn-success']) !!}
                                </center>
                                    
                                
                            {!! Form::close() !!}
                    </div>
                        
                @endif

                
    </section>
@endsection

@section('js')
<script>
    $('.textarea-academic').trumbowyg();
</script>
@endsection