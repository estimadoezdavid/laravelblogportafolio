@extends('layouts.app')

@section('header')

@endsection

@section('content')
<section class="main-section" id="service">

                    <div class="container">
                        @include('flash::message')
                            {!! Form::open(['route' => ['Tag.update', $tag->id], 'method' => 'PUT', 'files' => true]) !!}
                               
                               <div class="form-group">
                                    {!! Form::label('name_project', 'Nombre') !!}
                                    {!! Form::text('name', $tag->name, ['class' =>'form-control textarea-personal', 'requerid', 'placeholder' => 'Nombre del tag']) !!}
                                </div>
                                <center>
                                    {!! Form::submit('Guardar', ['class' =>'btn btn-success']) !!}
                                </center>
                                    
                                
                            {!! Form::close() !!}
                    </div>                
    </section>
@endsection

@section('js')
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $('.textarea-academic').trumbowyg();
</script>
@endsection