@extends('layouts.app')

@section('title')
	Mi información
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">@if (Auth::guest())
                        @else
                            <a data-toggle="modal" data-target="#myModal" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="btn fa fa-pencil pull-right" aria-hidden="true"></i></a>
                        @endif
                	<center>
                		<h2>
                			Informacion personal
                		</h2>
                		
                	</center>
                </div>
                <div class="panel-body">

                    <strong>{!! $information->personal_information !!}</strong>
                </div>
                <div class="panel-footer">
                	Ultima actualizacion {!! $information->updated_at->diffForHumans() !!}
                    
                </div>
            </div>
        </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><center><h2>Informacion academica</h2></center></div>
                <div class="panel-body">
                	
                    <strong>{!! $information-> acedemic_information !!}</strong>
                </div>
                 <div class="panel-footer">
                	Ultima actualizacion {!! $information->updated_at->diffForHumans() !!}
                    
                </div>
            </div>
        </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><center><h2>Mas sobre mi</h2></center></div>
                <div class="panel-body">
                	
                    <strong>{!! $information-> more_informacion !!}</strong>
                </div>
                 <div class="panel-footer">
                	Ultima actualizacion {!! $information->updated_at->diffForHumans() !!}
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar mi Informacion</h4>
        </div>
        <div class="modal-body">
            {!! Form::open(['route' => ['Information.update', $information->id], 'method' => 'PUT']) !!}
                

                <div class="form-group">
                    {!! Form::label('personal_information', 'Informacion Personal') !!}
                    {!! Form::textarea('personal_information', $information->personal_information, ['class' =>'form-control textarea-personal', 'requerid', 'placeholder' => 'Nombre de la peticion']) !!}
                </div>
                                        
                <div class="form-group">
                    {!! Form::label('acedemic_information', 'Informacion Academica') !!}
                    {!! Form::textarea('acedemic_information', $information->acedemic_information, ['class' =>'form-control textarea-academic', 'requerid', 'placeholder' => 'Descripcion de la peticion']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('more_informacion', 'Mas informacion') !!}
                    {!! Form::textarea('more_informacion', $information->more_informacion, ['class' =>'form-control textarea-more', 'requerid', 'placeholder' => 'Descripcion de la peticion']) !!}
                </div>

                <div class="form-group">
                    {!! Form::hidden('blog_id', $information->blog_id) !!}
                </div>

                <div class="modal-footer">
                    {!! Form::submit('Guardar', ['class' =>'btn btn-success col-md-offset-5']) !!}
                </div>

            {!! Form::close() !!}
        </div>
      </div>
      
    </div>
  </div>
@endsection

@section('js')
<script>
    $('.textarea-academic').trumbowyg({
       lang: 'es' 
    });
    $('.textarea-personal').trumbowyg({
        lang: 'es'
    });
    $('.textarea-more').trumbowyg({
        lang: 'es'
    });
</script>
@endsection