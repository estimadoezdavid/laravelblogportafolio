<html lang="en" class=" ">
    <head>
        <script>
        window.Laravel = {"csrfToken":"36RMPSmWM8QXzTHamkaUucLwdWcHrhDJPG8vUDvu"}    </script>
        <meta charset="utf-8" />
        <title> Blog de David | My informacion</title>
        <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
        <link rel="icon" href="http://localhost:8000/images/prueba.png">
        <link rel="stylesheet" href="http://localhost:8000/CSS/app.v1.css" type="text/css"/>
        <link rel="stylesheet" href="http://localhost:8000/CSS/landing.css" type="text/css"/>
        <link rel="stylesheet" href="http://localhost:8000/css/trumbowyg.min.css" type="text/css"/>
        <!--Chosen -->
<link rel="stylesheet" href="http://localhost:8000/plugins/Chosen-1.8.0/chosen.css">

<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/font-awesome.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/responsive.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/animate.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet"  href="{{asset('plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/Trumbowyg/ui/trumbowyg.css')}}">

<!--Chosen -->
<link rel="stylesheet" href="{{asset('plugins/Chosen-1.8.0/chosen.css')}}">

<!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->
<script type="text/javascript" src="{{asset('plugins/Trumbowyg/langs/es.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.1.8.3.min.js')}}"></script>
<script src="{{asset('plugins/Trumbowyg/trumbowyg.min.js')}}"></script>
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
    </head>
    <body class="" data-spy=scroll data-target=#header >
        <!-- header -->
        <header id="header" class="navbar navbar-fixed-top bg-primary dk" data-spy="affix" data-offset-top="1">
            <div class="container">
                <div class="navbar-header">
                    <a href="#" class="navbar-brand m-r-lg">
                        <img src="http://localhost:8000/images/logo_white.png" class="m-r-sm">
                        <span class="text-lt"> My Blog </span>
                    </a>
                    <button class="btn btn-primary btn-emtpy visible-xs" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="{{route('Information.index')}}">Informacion</a></li>
                        <li><a href="{{route('Article.index')}}">Articulos</a></li>
                        <li><a href="{{route('Portafolio.index')}}">Portafolio</a></li>
                        @if (Auth::guest())
                            <li class="pull-right"><a href="{{route('login')}}" >Ingresar</a></li>
                        @else
                        
                        <li><a href="{{ route('Category.index') }}">Categorias</a></li>
                            <li><a href="{{ route('Tag.index') }}">Tags</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle pull-right" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} {{ Auth::user()->last_name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                              <!-- <li><a href="{{ route('User.index') }}">Mi perfil</a></li> -->
                                <li>
                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        Cerrar session
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>

                        </li>
                        @endif
                                            </ul>
                    <ul class="nav navbar-nav navbar-right">
                                                    
                                            </ul>
                </div>
            </div>
        </header>

<body>
    <div id="app"><br><br>
        @yield('content')

    </div>

    <!-- Scripts -->
    <script src="http://localhost:8000/JS/app.v1.js"></script>
        <script src="http://localhost:8000/JS/appear/jquery.appear.js"></script>
        <script src="http://localhost:8000/JS/landing.js"></script>
        <script src="http://localhost:8000/JS/app.plugin.js"></script>
        <script src="http://localhost:8000/JS/trumbowyg.min.js"></script>
        <script src="http://localhost:8000/plugins/Chosen-1.8.0/chosen.jquery.js"></script>
</script>

 <!-- Template Specisifc Custom Javascript File  PARA EL PRELOADER-->
    <script src="{{asset('tema4/js/custom.js')}}"></script>
    @yield('js')
  <script src="{{asset('plugins/Chosen-1.8.0/chosen.jquery.js')}}"></script>>
            </body>
</html>
