@extends('layouts.app')

@section('title')
	Mi información
@endsection

@section('content')

@if (Auth::guest())
@else
    <div class="pull-right">
         <a data-toggle="modal" data-target="#New"><i class="btn fa fa-plus fa-4x" aria-hidden="true"></i></a>
    </div>
@endif
    <section class="main-section" id="service">
         
        <div class="container">
            <div class="row">
                @if(count($errors) >0)
        <div class="alert alert-danger" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
                
                @foreach ($image as $image)
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                @if (Auth::guest())
                                    <h3><a href="{{route('Portafolio.edit', $image->id)}}">{{$image->portafolio->name_project}}</a></h3>
                                @else
                                    <h3><a href="#">{{$image->portafolio->name_project}}</a></h3>
                                @endif
                                
                            </div>
                            <div class="panel-body">
                            	<center>
                            		<img src="/images/portafolio/{{$image->name}}" height="350">	
                            	</center>
                                
                                <div>
                                    <p>{!! $image->portafolio->description_project !!}</p>
                                </div>
                            </div>
                            @if (Auth::guest())
                                @else
                                    <div class="panel-footer">
                                        <center>
                                            <a href="{{route('Portafolio.edit', $image->id)}}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="btn fa fa-pencil fa-2x" aria-hidden="true"></i></a>
                                            <a href="{{route('Portafolio.destroy', $image->id)}}" data-toggle="tooltip" data-placement="bottom" title="Eliminar"><i class="btn fa fa-trash fa-2x" aria-hidden="true"></i></a>
                                            <a href="{{$image->portafolio->url}}" data-toggle="tooltip" data-placement="bottom" title="{{$image->portafolio->url}}"><i class="btn fa fa-gitlab fa-2x" aria-hidden="true"></i></a>
                                            <div class="pull-right">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i> {!! $image->portafolio->updated_at->diffForHumans() !!}
                                        </div>
                                        </center>
                                        
                                    </div>
                                @endif
                            
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

<!--
    Mis Modelas
-->
<!-- Agregar al portafolio -->
  <div class="modal fade" id="New" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Nuevo folder</h4>
        </div>
        <div class="modal-body">
            {!! Form::open(['route' => 'Portafolio.store', 'method' => 'POST', 'files' => true]) !!}
                

                <div class="form-group">
                    {!! Form::label('name_project', 'Nombre') !!}
                    {!! Form::text('name_project', null, ['class' =>'form-control textarea-personal', 'requerid', 'placeholder' => 'Nombre del portafolio']) !!}
                </div>
                                        
                <div class="form-group">
                    {!! Form::label('description_project', 'Descripcion del portafolio') !!}
                    {!! Form::textarea('description_project', null, ['class' =>'form-control textarea-academic', 'requerid', 'placeholder' => 'Descripcion del portafolio']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description_project', 'URL') !!}
                    {!! Form::textarea('url', null, ['class' =>'form-control textarea-academic', 'requerid', 'placeholder' => 'Por favor pegue la url del proyecto']) !!}
                </div>

                <div class="form-group">
                    {!! Form::hidden('blog_id', 1) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('image','Imagen') !!}
                    {!! Form::file('image', ['requerid']) !!}
                </div>
                
                
            
        </div>
        <div class="modal-footer">
            <center>
                {!! Form::submit('Guardar', ['class' =>'btn btn-success']) !!}
            </center>
          {!! Form::close() !!}
        </div>
      </div>
      
    </div>
  </div>

@endsection

@section('js')
<script>

</script>
@endsection