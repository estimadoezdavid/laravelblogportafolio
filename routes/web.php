<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

		
	Route::group(['prefix' => 'David-Moreno'], function(){

		Route::get('/', function () {
    		return view('welcome');
		});
		Route::get('/home', 'HomeController@index');
		Route::resource('Article', 'Article\ArticleController');
		Route::resource('Category', 'Category\CategoryController');
		Route::resource('Information', 'Information\InformationController');
		Route::resource('Portafolio', 'Portafolio\PortafolioController');
		Route::resource('Tag', 'Tag\TagController');
		Route::resource('User', 'Auth\UserController');

		Route::get('Portafolio/{Portafolio}',[
			'as'	=>	'Portafolio.destroy',
			'uses'	=>	'Portafolio\PortafolioController@destroy'
		]);

		Route::get('Tag/{Tag}',[
			'as'	=>	'Tag.destroy',
			'uses'	=>	'Tag\TagController@destroy'
		]);

		Route::get('Category/{Category}',[
			'as'	=>	'Category.destroy',
			'uses'	=>	'Category\CategoryController@destroy'
		]);

		Route::get('Article/{Article}',[
			'as'	=>	'Article.destroy',
			'uses'	=>	'Article\ArticleController@destroy'
		]);

		Route::get('Categories/{name}',[
			'as'	=>	'Category.search',
			'uses'	=>	'Article\ArticleController@searchCategory'
		]);

		Route::get('Tags/{name}',[
			'as'	=>	'Tag.search',
			'uses'	=>	'Article\ArticleController@searchTag'
		]);

		
		
});
Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
